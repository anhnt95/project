-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 16, 2019 lúc 04:20 PM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_filterweb`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cates`
--

CREATE TABLE `cates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `cates`
--

INSERT INTO `cates` (`id`, `name`, `keywords`, `description`, `parent_id`, `display`, `created_at`, `updated_at`) VALUES
(1, 'Sữa rửa mặt', '', 'Làm sạch da', '0', '0', NULL, NULL),
(2, 'Tẩy trang', '', 'Làm sạch da', '0', '0', NULL, NULL),
(3, 'Kem chống nắng', '', 'Dưỡng da', '0', '0', NULL, NULL),
(4, 'Kem dưỡng da', '', 'Bảo vệ da', '0', '0', NULL, NULL),
(5, 'Serum dưỡng da ', '', 'Dưỡng da', '0', '0', NULL, NULL),
(6, 'Son', '', 'Trang điểm môi', '0', '0', NULL, NULL),
(7, 'Cushion', '', 'Trang điểm nền', '0', '0', NULL, NULL),
(8, 'Kem nền', '', 'Trang điểm nền', '0', '0', NULL, NULL),
(9, 'Phấn phủ', '', 'Trang điểm nền', '0', '0', NULL, NULL),
(10, 'Che khuyết điểm', '', 'Trang điểm nền', '0', '0', NULL, NULL),
(11, 'Highlight/Bronzer', '', 'Trang điểm nền', '0', '0', NULL, NULL),
(12, 'Sữa tắm', '', 'Chăm sóc cơ thể', '0', '0', NULL, NULL),
(13, 'Kem dưỡng thể', '', 'Chăm sóc cơ thể', '0', '0', NULL, NULL),
(14, 'Serum dưỡng tóc', '', 'Chăm sóc tóc', '0', '0', NULL, NULL),
(15, 'Thực phẩm chức năng', '', 'Chăm sóc cơ thể', '0', '0', NULL, NULL),
(17, 'test', 'teset', 'teset', '4', 'on', '2019-01-16 11:34:00', '2019-01-16 14:52:46');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_09_092551_cates', 1),
(4, '2018_12_09_101455_products', 1),
(5, '2018_12_09_103959_product_image', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skin_tone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skin_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spf_pa` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `cate_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `best_sellers` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `image`, `keywords`, `skin_tone`, `skin_type`, `age`, `faculty`, `brand`, `origin`, `link`, `spf_pa`, `user_id`, `cate_id`, `created_at`, `updated_at`, `best_sellers`) VALUES
(1, 'Sữa Rửa Mặt Làm Sạch Và Săn Chắc Da LOreal Paris Revitalift 100ml', '139000', 'Thích hợp cho mọi loại da. Chống lão hóa', 'source/productpage/img/1.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'da thường, da hỗn hợp, da lão hóa, da mụn, da dầu', 'Từ 20 - 30', 'Làm sạch da', 'LOreal ', 'Pháp', 'https://shorten.asia/rZTQPepz', '', 1, 1, NULL, NULL, NULL),
(2, 'Sữa Rửa Mặt Làm Sạch Và Trắng Mịn Da LOreal Paris White Perfect 100ml', '89000', 'Thích hợp cho mọi loại da. Dưỡng trắng, giảm thâm nám', 'source/productpage/img/2.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'da thường, da hỗn hợp, da lão hóa, da mụn, da dầu', 'Từ 30 - 40', 'Làm sạch da, dưỡng trắng', 'LOreal ', 'Pháp', 'https://shorten.asia/dfUXPpCr', '', 1, 1, NULL, NULL, NULL),
(3, 'Sữa Rửa Mặt Tạo Bọt Ngừa Mụn Se Nhỏ Lỗ Chân Lông Vichy Normaderm Deep Cleansing Foaming Cream 125ml', '269000', 'Phù hợp với mọi loại da, kể cả da nhạy cảm. Ngăn ngừa mụn, loại bỏ bụi bẩn', 'source/productpage/img/3.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'da thường, da hỗn hợp, da lão hóa, da mụn, da dầu', 'Từ 20 -30', 'Làm sạch da, trị mụn', 'Vichy', 'Pháp', 'https://shorten.asia/DVdjnTXs', '', 1, 1, NULL, NULL, NULL),
(4, 'Sữa Rửa Mặt Tạo Bọt Dạng Mút Ngừa Ô Nhiễm Cho Da Thường, Hỗn Hợp & Nhạy Cảm Vichy Purete Thermale 50ml', '209000', 'Phù hợp da thường, da hỗn hợp và da nhạy cảm. Làm sạch và mềm da', 'source/productpage/img/4.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'da thường, da hỗn hợp và da nhạy cảm', 'Từ 20 -30', 'Làm sạch da', 'Vichy', 'Pháp', 'https://shorten.asia/HSmDvUQW', '', 1, 1, NULL, NULL, NULL),
(5, 'Gel Rửa Mặt Ngăn Ngừa Mụn Vichy Normaderm Gel 100ml', '219000', 'Phù hợp với mọi loại da, kể cả da nhạy cảm. Hiệu quả ngăn ngừa mụn tăng 39% sau một thời gian ngắn sử dụng', 'source/productpage/img/5.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'da thường, da hỗn hợp, da lão hóa, da mụn, da dầu', 'Từ 20 -30', 'Làm sạch da, trị mụn', 'Vichy', 'Pháp', 'https://shorten.asia/XseGaGU4', '', 1, 1, NULL, NULL, NULL),
(6, 'Sữa Rửa Mặt Tạo Bọt Dưỡng Trắng Da Vichy Ideal White Brightening Deep Cleansing Foam 100ml', '449000', 'Phù hợp với mọi loại da, kể cả da nhạy cảm. Giảm thâm nám, giảm tiết dầu, bóng dầu', 'source/productpage/img/6.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'da thường, da hỗn hợp, da lão hóa, da mụn, da dầu', 'Từ 20 -30', 'Làm sạch da, dưỡng trắng', 'Vichy', 'Pháp', 'https://shorten.asia/8PHqeph3', '', 1, 1, NULL, NULL, NULL),
(7, 'Sữa Rửa Mặt Tự Tạo Bọt Dành Cho Da Khô Senka Speedy Moist Touch 150ml', '99000', 'Loại da phù hợp: Da khô và da thường. Làm sạch bụi bẩn và bã nhờn sâu trong lỗ chân lông', 'source/productpage/img/7.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'Da khô và da thường', 'từ 30 - 40', 'Làm sạch da', 'Senka', 'Nhật ', 'https://shorten.asia/W7HAqRgC', '', 1, 1, NULL, NULL, NULL),
(8, 'Sữa Rửa Mặt Tạo Bọt Chiết Xuất Tơ Tằm Trắng Senka Perfect Whip 120g', '79000', 'Loại da phù hợp: Thích hợp cho da dầu và da thường. Làm sạch bụi bẩn và bã nhờn sâu trong lỗ chân lông', 'source/productpage/img/8.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'da dầu và da thường', 'Từ 30 - 40', 'Làm sạch da', 'Senka', 'Nhật ', 'https://shorten.asia/w7wYq8s7', '', 1, 1, NULL, NULL, 1),
(9, 'Sản Phẩm Làm Sạch Da Đa Năng JEJU VOLCANIC LAVA PORE DAILY MASK FOAM 150M', '244300', 'Phù hợp với mọi loại da, kể cả da nhạy cảm. Làm sạch và thanh lọc da.', 'source/productpage/img/9.jpg', 'Sữa rửa mặt', 'rất sáng, sáng, trung bình, nâu, ngăm', 'da thường, da hỗn hợp, da lão hóa, da mụn, da dầu', 'dưới 18', 'Làm sạch da', 'The Face Shop', 'Hàn', 'https://shorten.asia/MeB97MdQ', '', 1, 1, NULL, NULL, NULL),
(10, 'Sữa Rửa Mặt Kiểm Soát Nhờn THE FRESH FOR MEN MILD FOAMING CLEANSER', '329000', 'Phù hợp với mọi loại da, kể cả da nhạy cảm. Kiểm soát nhờn, hạn chế tình trạng bóng dầu.', 'source/productpage/img/10.jpg', 'Sữa rửa mặt', 'sáng, trung binh, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 30 -40', 'Làm sạch da, giảm nhờn', 'The Face Shop', 'Hàn', '', '', 1, 1, NULL, NULL, 1),
(11, 'Sữa Rửa Mặt Bổ Sung Ẩm MANGO SEED CLEANSING FOAM 300ML', '332000', 'Phù hợp với mọi loại da, kể cả da nhạy cảm. Làm sạch và thanh lọc da.', 'source/productpage/img/11.jpg', 'Sữa rửa mặt', 'sáng, trung binh, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Trên 40', 'Làm sạch da, giảm nhờn', 'The Face Shop', 'Hàn', 'https://shorten.asia/8GCd7K7U', '', 1, 1, NULL, NULL, NULL),
(12, 'Nước Tẩy Trang Mắt Và Môi LOreal Paris Gentle Care 125ml', '169000', 'Phù hợp với mọi loại da, kể cả da nhạy cảm. Tẩy trang mắt, môi', '', 'Nước tẩy trang', 'sáng, trung binh, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 20 - 30', 'Làm sạch da', 'LOreal', 'Pháp', 'https://shorten.asia/QAAU5aTC', '', 1, 2, NULL, NULL, NULL),
(13, 'Dầu Tẩy Trang Senka All Clear Oil Whip 150ml', '180000', 'Phù hợp với mọi loại da, kể cả da nhạy cảm', '', 'Nước tẩy trang', 'sáng, trung binh, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Trên 40', 'Làm sạch da', 'Senka', 'Pháp', 'https://shorten.asia/DB2n1hee', '', 1, 2, NULL, NULL, NULL),
(14, 'Nước Tẩy Trang LOreal Paris 3-In-1 Micellar Water 400ml', '109000', 'Phù hợp với cả da nhạy cảm', '', 'Nước tẩy trang', 'sáng, trung binh, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 30 -40 ', 'Làm sạch da', 'LOreal', 'Pháp', 'https://shorten.asia/mHEUv7KU', '', 1, 2, NULL, NULL, NULL),
(15, 'Nước Làm Sạch Sâu Và Tẩy Trang Dành Cho Da Dầu Nhạy Cảm La Roche-Posay Effaclar Micellar Water Ultra 200ml', '289000', 'Phù hợp cho da dầu. Làm sạch sâu vượt trội và kiểm soát dầu cho da dầu nhạy cảm', '', 'Nước tẩy trang', 'sáng, trung binh, nâu, ngăm', 'da dầu, da nhạy cảm', 'Từ 20 - 30', 'Làm sạch da', 'La Roche-Posay', 'Pháp', 'https://shorten.asia/P79wY92m', '', 1, 2, NULL, NULL, NULL),
(16, 'Nước Làm Sạch Sâu Và Tẩy Trang Dành Cho Da Nhạy Cảm Micellar Water Ultra Sensitive Skin 400ml', '389000', 'Phù hợp cho mọi loại da, kể cả da nhạy cảm. Làm dịu da, giảm kích ứng và chống oxi hóa', '', 'Nước tẩy trang', 'sáng, trung binh, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Dưới 18', 'Làm sạch da', 'La Roche-Posay', 'Pháp', 'https://shorten.asia/R46225fp', '', 1, 2, NULL, NULL, NULL),
(17, 'Sữa Rửa Mặt Tẩy Trang 3 Tác Dụng Vichy Purete Thermale 3 In 1 200ml', '509000', 'Phù hợp da thường, da hỗn hợp và da nhạy cảm. Sản phẩm dạng kem có 3 công dụng: sữa rửa mặt, làm mềm da và tẩy trang mắt môi', '', 'Nước tẩy trang', 'sáng, trung binh, nâu, ngăm', 'da thường, da hỗn hợp, da nhạy cảm', 'Từ 20 - 30', 'Làm sạch da', 'Vichy', 'Pháp', 'https://shorten.asia/M7GRXyyN', '', 1, 2, NULL, NULL, NULL),
(18, 'Nước Tẩy Trang Đa Công Dụng Maybelline Micellar Water 400ml', '159000', 'Dành cho mọi loại da. Sử dụng được cho cả mắt, mặt và môi', '', 'Nước tẩy trang', 'sáng, trung binh, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 20 - 30', 'Làm sạch da', 'Maybelline', 'Mỹ ', 'https://shorten.asia/CFppGAqS', '', 1, 2, NULL, NULL, NULL),
(19, 'Nước Tẩy Trang Mắt Và Môi Maybelline 40ml ', '69000', 'Dành cho mọi loại da, kể cả làn da nhạy cảm. Tẩy trang mắt, môi', '', 'Nước tẩy trang', 'sáng, trung bình, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 20 -30', 'Làm sạch da', 'Maybelline', 'Mỹ ', 'https://shorten.asia/6tGpwrjB', '', 1, 2, NULL, NULL, 1),
(20, 'Tẩy Trang TheFaceShop Chia Seed Fresh Lip & Eye Make Up Remover 100ml', '256000', 'Phù hợp mọi loại da. Tẩy trang mắt, môi', '', 'Nước tẩy trang', 'sáng, trung bình, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 20 - 30', 'Làm sạch da', 'The Face Shop', 'Hàn', 'https://shorten.asia/nJkkMJdY', '', 1, 2, NULL, NULL, 1),
(21, 'Dầu Tẩy Trang Thanh Lọc Da OIL SPECIALIST PORE CLEAN CLEANSING OIL', '440000', 'Phù hợp mọi loại da', '', 'Nước tẩy trang', 'sáng, trung bình, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 20 - 30', 'Làm sạch da', 'The Face Shop', 'Hàn', 'https://shorten.asia/AgYaePXa', '', 1, 2, NULL, NULL, 1),
(22, 'Kem Chống Nắng Dạng Gel Chiết Xuất Tơ Tằm Trắng Senka Perfect UV Gel 80g', '149000', 'Phù hợp mọi loại da. SPF 50+, PA++++', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 20 - 30', 'Bảo vệ da, dưỡng da', 'Senka', 'Nhật', 'https://shorten.asia/bWYpFhBN', '50+++', 1, 3, NULL, NULL, 1),
(23, 'Kem Chống Nắng Không Gây Nhờn Rít SPF50+ UVB & UVA La Roche-Posay Anthelios Ultra Light Travel Size 30ml', '269000', 'Dành cho da thường đến da khô, kể cả da nhạy cảm. SPF 50+, UVA/UVB', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhạy cảm', 'Dưới 18', 'Bảo vệ da, dưỡng da', 'La Roche-Posay', 'Pháp', 'https://shorten.asia/mAvB93eh', '50+++', 1, 3, NULL, NULL, 1),
(24, 'Kem Chống Nắng Không Gây Nhờn Rít SPF50+ UVB & UVA Dành Cho Da Nhạy Cảm La Roche-Posay Anthelios XL Fluid 50ml', '429000', 'Phù hợp da thường, da hỗn hợp và da nhạy cảm. SPF 50+, UVA/UVB', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da thường, da hỗn hợp, da nhạy cảm', 'Từ 20 - 30', 'Bảo vệ da, dưỡng da', 'La Roche-Posay', 'Pháp', 'https://shorten.asia/7b2Z5yeG', '50+++', 1, 3, NULL, NULL, 1),
(25, 'Xịt Chống Nắng, Giúp Kiểm Soát Nhờn Và Bảo Vệ Da Trước Tia UVB & UVA La Roche-Posay Anthelios Invisible Fresh Mist 75ml', '429000', 'Phù hợp mọi loại da. SPF 50+, UVA/UVB. Kiểm soát dầu da mặt', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 20 - 30', 'Bảo vệ da, dưỡng da, giảm nhờn', 'La Roche-Posay', 'Pháp', 'https://shorten.asia/hC13pTGe', '50+++', 1, 3, NULL, NULL, 1),
(26, 'Kem Chống Nắng Giúp Làm Sáng Và Cải Thiện Sắc Tố Da La Roche-Posay Uvidea XL Tone-Up Light Cream 30ml', '469000', 'Dành cho da thường đến khô/da hỗn hợp. Kem chống nắng làm sáng, cải thiện sắc tố da và nâng tông tức thì, dưỡng trắng da.', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da khô, da hỗn hợp', 'Từ 20 - 30', 'Bảo vệ da, dưỡng da', 'La Roche-Posay', 'Pháp', 'https://shorten.asia/GfxKU9BV', '50+++', 1, 3, NULL, NULL, NULL),
(27, 'Kem Chống Nắng Bảo Vệ Da LOreal Paris UV Perfect SPF50+ PA++++ 30ml', '115000', 'Phù hợp với mọi loại da. Dưỡng ẩm, dưỡng trắng, trang điểm, kiềm dầu', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da thường, da lão hóa, da hỗn hợp, da dầu, da mụn', 'Từ 30 - 40', 'Bảo vệ da, dưỡng da', 'LOreal', 'Pháp', 'https://shorten.asia/vjTa1Aa8', '50+++', 1, 3, NULL, NULL, NULL),
(28, 'Kem Chống Nắng Dưỡng Ẩm AHC Natural Perfection 50ml', '499000', 'Phù hợp với mọi loại da. Dưỡng ẩm', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô', 'Từ 30 - 40', 'Bảo vệ da, dưỡng da', 'AHC', 'Hàn', 'https://shorten.asia/JYWpcJFg', '50+++', 1, 3, NULL, NULL, NULL),
(29, 'Sữa Chống Nắng Bảo Vệ Hoàn Hảo Anessa Perfect UV Sunscreen Skincare Milk 20ml', '169000', 'Phù hợp với mọi loại da. ', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'Da thường, da khô, da lõa hóa, da nhờn', 'Từ 20 - 30', 'Bảo vệ da, dưỡng da', 'Anessa', 'Nhật', 'https://shorten.asia/aCTFJEHQ', '50+++', 1, 3, NULL, NULL, NULL),
(30, 'Xịt Chống Nắng Skin Aqua Sara-Fit UV Spray SPF50+ PA++++ 50g', '113000', 'Phù hợp với mọi loại da. ', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'Da thường, da khô, da lõa hóa, da nhờn', 'Từ 30 - 40', 'Bảo vệ da, dưỡng da', 'Skin Aqua', 'Nhật', 'https://shorten.asia/eFGTAd7k', '50+++', 1, 3, NULL, NULL, NULL),
(31, 'Sữa Chống Nắng Hằng Ngày Dưỡng Trắng Cho Da Dầu Sunplay Skin Aqua Clear White SPF 50+ PA++++ 55g', '134000', 'Phù hợp với da dầu. Dưỡng trắng', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da dầu', 'Từ 30 - 40', 'Bảo vệ da, dưỡng da', 'Skin Aqua', 'Nhật', 'https://shorten.asia/761YHFV7', '50+++', 1, 3, NULL, NULL, NULL),
(32, 'Sữa Chống Nắng Hằng Ngày Dưỡng Trắng Cho Da Dầu Sunplay Skin Aqua Clear White SPF 50, PA++++ 25g', '74000', 'Phù hợp với da dầu. Dưỡng trắng', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'da dầu', '18-35', 'Bảo vệ da, dưỡng da', 'Skin Aqua', 'Nhật', 'https://shorten.asia/TCaeS9T4', '50+++', 1, 3, NULL, NULL, NULL),
(33, 'Kem Chống Nắng Kiểm Soát Nhờn NATURAL SUN ECO NO SHINE HYDRATING SUN CREAM SPF40 PA+++ 50ml', '481000', 'Phù hợp với mọi loại da. ', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'Da thường, da khô, da lõa hóa, da nhờn', 'Từ 30 - 40', 'Bảo vệ da, dưỡng da, giảm nhờn', 'The Face Shop', 'Hàn', 'https://shorten.asia/KrftT8YR', '50+++', 1, 3, NULL, NULL, NULL),
(34, 'Kem Chống Nắng Đa Chức Năng NATURAL SUN ECO POWER LONG-LASTING SUN CREAM SPF50+ PA+++ 80ML', '801000', 'Phù hợp với mọi loại da. ', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'Da thường, da khô, da lõa hóa, da nhờn', 'Từ 30 - 40', 'Bảo vệ da, dưỡng da', 'The Face Shop', 'Hàn', 'https://shorten.asia/46BP8uKy', '40+++', 1, 3, NULL, NULL, NULL),
(35, 'Sữa Chống Nắng Hạ Nhiệt Làn Da NATURAL SUN ECO ICE AIR PUFF SUN SPF50+PA+++', '643000', 'Phù hợp với mọi loại da. ', '', 'Kem chống nắng', 'sáng, trung bình, nâu, ngăm', 'Da thường, da khô, da lõa hóa, da nhờn', 'Từ 30 - 40', 'Bảo vệ da, dưỡng da', 'The Face Shop', 'Hàn', 'https://shorten.asia/9BjnnPnG', '50+++', 1, 3, NULL, NULL, NULL),
(36, 'Sữa Dưỡng Ẩm Cao Cấp Làm Dịu Và Chống Nếp Nhăn AHC Preminum Hydra B5 Lotion 120ml', '525000', 'Phù hợp với mọi loại da. Dưỡng ẩm, chống nếp nhăn, phục hồi da', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'Da thường, da khô, da lõa hóa, da nhờn', 'Dưới 18', 'Dưỡng da, chống lão hóa, dưỡng ẩm', 'AHC', 'Hàn', 'https://shorten.asia/rgVRJ14m', '', 1, 4, NULL, NULL, NULL),
(37, 'Kem Dưỡng Da Nuôi Dưỡng Và Chống Nếp Nhăn AHC Premium Hydra B5 50ml', '950000', 'Phù hợp với mọi loại da. Dưỡng ẩm, chống nếp nhăn, phục hồi da', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Trên 40', 'Dưỡng da, chống lão hóa, dưỡng ẩm', 'AHC', 'Hàn', 'https://shorten.asia/s7qEwAgE', '', 1, 4, NULL, NULL, NULL),
(38, 'Kem Dưỡng Mắt Có Thể Sử Dụng Cho Da Mặt AHC The Pure Real Với Thành Phần Axit Hyaluronic Làm Trắng, Chống Vết Nhăn 30ml', '450000', 'Phù hợp với mọi loại da. Thành phần Axit Hyaluronic làm trắng, chống vết nhăn 30ml', '', 'Kem dưỡng da', ' trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Từ 30 - 40', 'Dưỡng da, trắng da, chống lão hóa', 'AHC', 'Hàn', 'https://shorten.asia/KAkasxND', '', 1, 4, NULL, NULL, NULL),
(39, 'Kem Dưỡng Da Mặt Hyaluronic Dưỡng Ẩm Dịu Da Aloe Vera AHC 50ml', '850000', 'Phù hợp với mọi loại da. Dưỡng ẩm, chống nếp nhăn, có khả năng cấp nước từ sâu bên trong, giúp phục hồi da khô, nhạy cảm.', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Trên 40', 'Dưỡng da, dưỡng ẩm', 'AHC', 'Hàn', 'https://shorten.asia/WUEz2ubP', '', 1, 4, NULL, NULL, NULL),
(40, 'Kem Dưỡng Ngày Giảm Nếp Nhăn Săn Chắc Da L’Oreal Revitalift SPF23 PA++ 50ml', '279000', 'Phù hợp với mọi loại da. Kết hợp cùng hoạt chất Pro-Retinol A và thành phần dưỡng ẩm giúp chống lão hóa hiệu quả, làm đầy bề mặt da giúp giảm thiểu và làm mờ các nếp nhăn. Những dấu hiệu lão ', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Từ 30 - 40', 'Dưỡng da, chống lão hóa, dưỡng ẩm, chống nắng', 'LOreal', 'Pháp', 'https://shorten.asia/v6UgZwRb', '23++', 1, 4, NULL, NULL, NULL),
(41, 'Kem Dưỡng Mắt Săn Chắc Và Chống Nhăn LOreal Revitalift Double Lifting Eye 15ml', '289000', 'Phù hợp với mọi loại da. Sản phẩm dành riêng cho vùng da mắt, vốn rất nhạy cảm, dễ để lại các nếp nhăn hay dấu hiệu thâm nám. Kem thẩm thấu nhanh giúp loại bỏ các sắc tố có hại, kéo căng vùng', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Từ 30 - 40', 'Dưỡng da, chống lão hóa, dưỡng ẩm', 'LOreal', 'Pháp', 'https://shorten.asia/vm2PG7h1', '', 1, 4, NULL, NULL, NULL),
(42, 'Kem Dưỡng Da Trắng Mượt Đều Màu Ban Đêm LOreal Paris White Perfect 50ml', '159000', 'Phù hợp với mọi loại da. Mang lại làn da khỏe mạnh và là bước quan trọng để bạn nuôi dưỡng làn da trắng sáng. Với công thức Melanin – Vanish TM dưỡng trắng tối ưu suốt cả đêm dài, giúp làn da', '', 'Kem dưỡng da', 'trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Từ 20 - 30', 'Dưỡng da, trắng da, dưỡng ẩm ', 'LOreal', 'Pháp', 'https://shorten.asia/snSCDbRJ', '', 1, 4, NULL, NULL, NULL),
(43, 'Kem Dưỡng Da TheFaceShop Dr.Belmeur Advanced Cica Recovery Cream 50ml', '953000', 'Phù hợp với mọi loại da. Kem dưỡng da với thành phần chính chứa axit hyaluronic, ceramide, panthenol được sử dụng công thức Phù hợp với mọi loại da. Skin-sync Rx giúp phục hồi và duy trì độ ẩ', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Từ 20 - 30', 'Dưỡng da, dưỡng ẩm, phục hồi da', 'The Face Shop', 'Hàn', 'https://shorten.asia/aamartbE', '', 1, 4, NULL, NULL, NULL),
(44, 'Kem Dưỡng Da TheFaceShop Dr.Belmeur Daily Repair Panthenol Soothing Gel Cream 100Ml', '886000', 'Phù hợp với mọi loại da. Dòng sản phẩm Dr. Belmeur Daily Repair rất phù hợp để đáp ứng nhu cầu cho làn da cần phục hồi sinh lực, nhanh chóng làm dịu và ổn định cho bề mặt da, cải thiện chức n', 'da thường, da khô, da nhơn, da mụn', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Dưới 18', 'Dưỡng da, dưỡng ẩm, phục hồi da', 'The Face Shop', 'Hàn', 'https://shorten.asia/jdeNGzea', '', 1, 4, NULL, NULL, NULL),
(45, 'Kem Dưỡng Ẩm Kiểm Soát Nhờn THEFACESHOP CHIA SEED NO SHINE HYDRATING CREAM', '719000', 'Phù hợp với mọi loại da. Sản phẩm giữ ẩm, làm mát, kiểm soát dầu, thích hợp cho da hỗn hợp, da dầu, da thừa dầu thiếu nước giúp cho làn da luôn tươi sáng, mềm mại, mịn màng.', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'Da khô', 'Dưới 18', 'Dưỡng da, trắng da, dưỡng ẩm, giảm nhờn', 'The Face Shop', 'Hàn', 'https://shorten.asia/SnAeC86m', '', 1, 4, NULL, NULL, NULL),
(46, 'Kem Dưỡng Da Trắng Hồng Căng Mọng Ban Ngày Vichy Ideal Lumiere Day Cream 50ml', '899000', 'Phù hợp với mọi loại da. Sản phẩm hỗ trợ tăng cường tuần hoàn máu, ngăn ngừa quá trình oxi hóa, dưỡng trắng tăng cường, cải thiện độ xỉn màu và tăng sắc hồng cho làn da, giúp da trắng hồng rạ', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Dưới 18', 'Dưỡng da', 'Vichy', 'Pháp', 'https://shorten.asia/NpS71vv3', '', 1, 4, NULL, NULL, NULL),
(47, 'Kem Dưỡng Giúp Giảm Mụn Giảm Bóng Dầu & Dưỡng Ẩm (Ngày) Vichy Normaderm Correcting Anti-Blemish Care 24h Hydration 50ml', '469000', 'Phù hợp với mọi loại da, kể cả làn da nhạy cảm. Da có nhiều bóng dầu và mụn, lỗ chân lông bị tắc nghẽn. ', '', 'Kem dưỡng da,', 'sáng, trung bình, nâu, ngăm', 'da mụn, da nhạy cảm', 'Từ 20 - 30', 'Dưỡng da, trị mụn', 'Vichy', 'Pháp', 'https://shorten.asia/gR66NNZ3', '', 1, 4, NULL, NULL, NULL),
(48, 'Kem Dưỡng Giúp Da Căng Mịn Và Săn Chắc Dành Cho Da Khô Za Perfect Solution Restoring Collagen Cream 40g', '209000', 'Phù hợp cho da khô, nhất là làn da khô, thiếu săn chắc, lão hóa.', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da khô, da lão hóa', 'Từ 20 -30', 'Dưỡng da, chống lão hóa, dưỡng ẩm', 'Za', 'Việt Nam', 'https://shorten.asia/DbFHqtJ3', '', 1, 4, NULL, NULL, NULL),
(49, 'Kem Dưỡng Làm Sáng Và Đều Màu Da Dành Ban Ngày Za True White Ex Day Cream 40m', '179000', 'Phù hợp với mọi loại da. Giúp làm trắng sáng da, giảm vết thâm nám, sạm màu & tàn nhang, bảo vệ da khỏi tia cực tím.', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Từ 20 -30', 'Dưỡng da, chống nắng', 'Za', 'Việt Nam', 'https://shorten.asia/Kp9u8jRw', '20++', 1, 4, NULL, NULL, NULL),
(50, 'Kem Dưỡng Làm Sáng Và Đều Màu Da Ban Đêm Za True White Ex Night Cream 40ml', '179000', 'Phù hợp với mọi loại da. Cung cấp độ ẩm và phục hồi các vùng da bị sạm nám cho bạn làn da căng mịn và trắng mượt.', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da khô, da nhơn, da mụn', 'Từ 20 -30', 'Dưỡng da, trắng da, dưỡng ẩm ', 'Za', 'Việt Nam', 'https://shorten.asia/4bkdV1ew', '', 1, 4, NULL, NULL, NULL),
(51, 'Kem Giảm Mụn Chuyên Biệt La Roche-Posay Effaclar A.I. 15ml', '449000', 'Phù hợp với da mụn. Sản phẩm chuyên biệt dành cho các đốm mụn. Mụn trứng cá. Mụn mủ viêm từ trung bình đến nặng vừa.', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da mụn', 'Từ 20 -30', 'Dưỡng da, trị mụn', 'La Roche-Posay', 'Pháp', 'https://shorten.asia/GrY36NPz', '', 1, 4, NULL, NULL, NULL),
(52, '', '379000', 'Loại da phù hợp: Da dầu & mụn trung bình đến nghiêm trọng (sưng viêm), da dễ bị vết thâm sau mụn và thường xuyên tái phát mụn. Đơn trị liệu và kết hợp với thuốc toa, thuốc uống. Phù hợp cho t', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da mụn', 'Từ 20 - 30', 'Dưỡng da, trị mụn', 'La Roche-Posay', 'Pháp', 'https://shorten.asia/vB5TbTe5', '', 1, 4, NULL, NULL, NULL),
(53, 'Dưỡng Chất(Serum) Dưỡng Da Giúp Ngăn Ngừa 10 Dấu Hiệu Lão Hóa & Làm Săn Chắc Làn Da Vichy Liftactiv Supreme Serum 30ml', '1579000', 'Phù hợp với mọi loại da, kể cả da nhạy cảm, da có dấu hiệu lão hóa. Loại sản phẩm: Tinh chất cải thiện nếp nhăn & trẻ hóa làn da. ', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da mụn, da nhờn, da hỗn hợp, da lão hóa', 'Từ 30 - 40', 'Dưỡng da, chống lão hóa, dưỡng ẩm', 'Vichy', 'Pháp', 'https://shorten.asia/MhuB5ssx', '', 1, 5, NULL, NULL, NULL),
(54, 'Dưỡng Chất Giàu Khoáng Chất Mineral 89 Giúp Da Sáng Mịn Và Căng Mượt Vichy Mineral 89 50ml', '889000', 'Phù hợp với mọi loại da, ngay cả làn da nhạy cảm. Loại sản phẩm: Tinh chất khoáng cô đặc giúp phục hồi & bảo vệ da. ', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da mụn, da nhờn, da hỗn hợp, da lão hóa', 'Từ 20 - 30', 'Dưỡng da, chống lão hóa, dưỡng ẩm', 'Vichy', 'Pháp', 'https://shorten.asia/3vUDgEhD', '', 1, 5, NULL, NULL, NULL),
(55, 'Serum Dưỡng Da Cao Cấp AHC Premium Phyto Complex Gel 30ml', '850000', 'Phù hợp với mọi loại da. Serum dưỡng da cao cấp AHC Premium Phyto Complex Gel 30ml ', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da mụn, da nhờn, da hỗn hợp, da lão hóa', 'Từ 20 - 30', 'Dưỡng da, dưỡng ẩm', 'AHC', 'Hàn', 'https://shorten.asia/QBp2QHFK', '', 1, 5, NULL, NULL, NULL),
(56, 'Serum Dưỡng Da Cao Cấp Làm Trắng Da AHC Premium Vital Complex C Whitening Vitamin C 30ml', '950000', 'Phù hợp với mọi loại da. Serum dưỡng da cao cấp làm trắng da AHC Premium Vital Complex C Whitening Vitamin C 30ml', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da thường, da mụn, da nhờn, da hỗn hợp, da lão hóa', 'từ 20 - 30', 'Dưỡng da, trắng da, dưỡng ẩm ', 'AHC', 'Hàn', 'https://shorten.asia/bfw5MxUv', '', 1, 5, NULL, NULL, NULL),
(57, 'Tinh Chất Giúp Da Trắng Sáng WHITE SEED BRIGHTENING SERUM', '737000', 'Phù hợp với mọi loại da. Tinh chất giúp cải thiện cấu trúc da và dưỡng trắng chuyên sâu, hiệu quả trong việc làm mờ đốm nâu và làm trắng sáng da nhờ phức hợp làm trắng từ hạt mầm hoa lupin và', '', 'Kem dưỡng da', 'sáng, trung bình, nâu ngăm', 'da thường, da mụn, da nhờn, da hỗn hợp, da lão hóa', 'Từ 20 - 30', 'Dưỡng da, trắng da, dưỡng ẩm ', 'The Face Shop', 'Hàn', 'https://shorten.asia/eG3s5zdh', '', 1, 5, NULL, NULL, NULL),
(58, 'Tinh Chất TheFaceShop Dr.Belmeur Clarifying Spot Calming Ampoule 22ml', '746000', 'Phù hợp với mọi loại da. Tinh chất giúp làm dịu và ổn định cho làn da cần chăm sóc đặc biệt khi có biểu hiện bong tróc, da yếu, đốm mụn đỏ', '', 'Kem dưỡng da', 'sáng, trung bình, nâu, ngăm', 'da mụn, da thường', 'Từ 30 - 40', 'Dưỡng da, trị mụn, giảm nhờn', 'The Face Shop', 'Hàn', 'https://shorten.asia/CdchCmkx', '', 1, 5, NULL, NULL, NULL),
(59, 'product-test', '200000000', 'product-test', 'product-test', 'product-test', 'product-test', 'product-test', '21', 'product-test', 'product-test', 'product-test', 'product-test', '50++', 1, 1, '2019-01-16 13:48:08', '2019-01-16 13:48:08', 0),
(62, 'product-test-1', '200000', 'CŨng hay', 'product-test', 'product-test', 'product-test', 'product-test', '21', 'product-test', 'product-test', 'product-test', 'product-test', '50++', 1, 4, '2019-01-16 14:52:14', '2019-01-16 14:52:14', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_image`
--

CREATE TABLE `product_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `user_name`, `password`, `email`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'nhu', '123456798', '', 0, NULL, NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cates`
--
ALTER TABLE `cates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cates_name_unique` (`name`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`),
  ADD KEY `products_user_id_foreign` (`user_id`),
  ADD KEY `products_cate_id_foreign` (`cate_id`);

--
-- Chỉ mục cho bảng `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_image_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `cates`
--
ALTER TABLE `cates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT cho bảng `product_image`
--
ALTER TABLE `product_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_cate_id_foreign` FOREIGN KEY (`cate_id`) REFERENCES `cates` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `product_image`
--
ALTER TABLE `product_image`
  ADD CONSTRAINT `product_image_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
