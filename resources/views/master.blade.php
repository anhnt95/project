<!DOCTYPE html>
<html>
<head>
	<title>Filter Cosmetic</title>
  <base href="{{asset('')}}">
	<link rel="stylesheet" href="{{asset('source/productpage/mycss.css')}}">
	<link href="https://fonts.googleapis.com/css?family=Mali" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
  <meta charset="utf-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
  <link href="https://fonts.googleapis.com/css?family=Thasadith" rel="stylesheet">
</head>
<body>
	 <div style="background-color: #F572AB;">
    <div class="row" style="margin-bottom: 0.5rem;padding: 20px;">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <span style="font-family: 'Mali', cursive; font-size: 2.5rem; color: white;">FILTER COSMETICS</span>
        <li class="dropdown"">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="text-decoration: none;">
            <div  style="margin-top: 12px;color: white;font-size: 1.5rem;"> Body&Hair </div>
          </a>
          <ul class="dropdown-content">
            <li><a href="cate/3">Kem chống nắng</a></li>
            <li><a href="cate/4">Kem dưỡng da</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="text-decoration: none;">
            <div  style="margin-top: 12px; margin-right: 30px; color: white;font-size: 1.5rem;"> Skin care </div>
          </a>
          <ul class="dropdown-content">
            <li><a href="cate/1">Sửa rửa mặt</a></li>
            <li><a href="cate/2">Tẩy trang</a></li>
            <li><a href="cate/5">Serum dưỡng da</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="text-decoration: none;">
            <div style="margin-top: 12px; margin-right: 30px; color: white;font-size: 1.5rem"> Make up</div> 
          </a>
          <ul class="dropdown-content">
            <li><a href="cate/6">Son</a></li>
            <li><a href="cate/7">Cushion</a></li>
            <li><a href="cate/8">Kem nền</a></li>
            <li><a href="cate/9">Phấn phủ</a></li>
            <li><a href="cate/10">Che khuyết điểm</a></li>
            <li><a href="cate/11">Hightlight/Bronzer</a></li>
          </ul>
        </li>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-10" style="margin-top: 1.5rem; margin-bottom: 1.5rem;">
        <form action="timkiem" method="post">
          <input type="hidden" name="_token" value="{{csrf_token()}}";>
          <input type="text" placeholder="Nhập sản phẩm..." name="keywords" class="a" >
          <button class="b" style="margin-left: 0.01rem; background-color: #fee5ed;">Tìm</button>
        </form>
      </div>
    </div>
  </div>
  
  @yield('product1') 
  <div class="r">
    <div style="float: left; margin-left: 230px;">
      <h3>Về chúng tôi</h3>
      <span style="color: red; font-size: 15px">Hotline tư vấn: 1800-5055</span><span>
      (1000đ/phút , 8-21h kể cả T7, CN)</span><br><br>
      <a href='gioithieu' target="_blank"><p>Giới thiệu về công ty</p></a>
      <a href='muahang' target="_blank"><p>Hướng dẫn mua hàng</p></a>
    </div>
    <div style="float: right; margin-right: 230px">
      <h3>Liên hệ</h3>
      <p>Copyright © 2018 filtercosmetics.vn</p>
      <p>Địa chỉ: kí túc xá khu A, ĐHQG tp.Hồ Chí Minh, khu phố 6,<br> phường Linh Trung, Quận Thủ Đức, tp Hồ Chí Minh</p>
      <p>Email liên hệ: filtercosmetic@gmail.com</p>
    </div>
  </div>
  <div class='lentop'>
<div>
<img src='https://1.bp.blogspot.com/-k6sikOdzFXQ/VwqCKDosmEI/AAAAAAAAKxE/nLxLhkTIO6o3iE5ZWmtxo2bf4QHdzPQNQ/s1600/top.png'/>
</div>
</div
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
 $('.sliderautoplay').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 1000,
});
</script>
<!-- DO NOT REMOVE THE 2 LINES -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!-- Custom JS -->
<script src="js/custom.js"></script>
<!-- JS Slick -->
  <script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
  $(function(){
  $(window).scroll(function () {
  if ($(this).scrollTop() > 100) $(".lentop").fadeIn();
  else $(".lentop").fadeOut();
  });
  $(".lentop").click(function () {
  $("body,html").animate({scrollTop: 0}, "slow");
  });
  });
</script>
<script src="js/jquery-1.11.1.min.js"></script>
<script>   
$('#myCarousel').carousel({ 
    interval:   4000    
});
</script>
</body>
</html>