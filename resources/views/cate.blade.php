@extends('master')
@section('product1')
<div class="container">
 <div class="row">
  <div class="col-sm-3 col-md-3">
    <div style="margin-top: 10px;">
      <center style="margin-right: 35px;">
        <h3 style="color: #000000; padding-bottom: 1rem;">Bộ lọc sản phẩm</h3>
      </center>
      <form action="loc" method="get">
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
        <input list="Loaisanpham" placeholder="Loại sản phẩm""Loaisanpham" class="a">
        <datalist id="Loaisanpham">
          <option value="Son"></option>
          <option value="Sửa rửa mặt"></option>
          <option value="Kem dưỡng da"></option>
          <option value="Kem chống nắng"></option>
          <option value="Tẩy trang"></option>
          <option value="Serum dưỡng da"></option>
          <option value="Kem nền"></option>
          <option value="Phấn phủ"></option>
          <option value="Che khuyết điểm"></option>
          <option value="Cushion"></option>
        </datalist><br><br>
        <input list="mauda" placeholder="Màu da" name="skin_tone" class="a">
        <datalist id="mauda">
          <option value="Sáng"></option>
          <option value="Trung bình"></option>
          <option value="Nâu"></option>
          <option value="Ngăm"></option>
        </datalist><br><br>
        <input list="Loaida"  placeholder="Loại da" name="skin_type" class="a">
        <datalist id="Loaida">
          <option value="Da thường"></option>
          <option value="Da lão hoá"></option>
          <option value="Da khô"></option>
          <option value="Da hỗn hợp"></option>
          <option value="Da dầu"></option>
          <option value="Da mụn"></option>
        </datalist><br><br>
        <input type="text" placeholder="Giá thấp nhất" name="min_price" class="a">
        <br><br>
        <input type="text" placeholder="Giá cao nhất" name="max_price" class="a">
        <br><br>
        <input list="tuoi"  placeholder="Tuổi" name="age" class="a">
        <datalist id="tuoi">
          <option value="Dưới 18"></option>
          <option value="Từ 20 - 30"></option>
          <option value="Từ 30 - 40"></option>
          <option value="Trên 40"></option>
        </datalist><br><br>
        <input list="thuonghieu" placeholder="Thương hiệu" name="brand" class="a">
        <datalist id="thuonghieu">
          <option value="Senka"></option>
          <option value="The Shop Face"></option>
          <option value="La Roche-Posay"></option>
          <option value="Vichy"></option>
          <option value="LOreal"></option>
          <option value="Maybelline"></option>
          <option value="AHC"></option>
          <option value="Anessa"></option>
          <option value="Skin Aqua"></option>
          <option value="ZA"></option>
        </datalist><br><br>
        <input list="xuatxu" placeholder="Xuất xứ" name="origin" class="a">
        <datalist id="xuatxu">
          <option value="Nhật"></option>
          <option value="Mỹ"></option>
          <option value="Pháp"></option>
          <option value="Hàn"></option>
          <option value="Anh"></option>
          <option value="Việt Nam"></option>
        </datalist><br><br>

        <button style=" background-color: #fee5ed; border-color: #fee5ed;" class="a">Tìm kiếm</button>
      </form>
    </div>
  </div>
  <div class="col-sm-9">
   <div style="margin-top: 1rem;">
    <h3 style="text-align: center; color: #000000;">Các sản phẩm đề xuất</h3>
    @foreach($products as $p)
    <div class='col-sm-4 thumbnail' style='margin-right: 1.5rem; width: 30%;'>
      <div class='f'>
        <img src='{{$p->image}}' class='e' style="border-bottom: 1px solid;">
        <div class='caption'>
          <p>{{$p->name}}</p>
          <div>
          <span style='color: #CD3C0C'>{{$p->price}}&nbsp VNĐ</span><a href='details/{{$p->id}}'  target='_blank'>
            <button style=" background-color: #fee5ed; border-color: #fee5ed;border-radius: 5px;border: 1px solid #F790FF;float: right;">Chi tiết</button>          
          </a>
        </div>
        </div>
      </div>
    </div>  
    @endforeach
  </div>
</div></div><div style="float: right;">{{$products->links() }}</div></div>
<br>
<h3 style="color: #000000; text-align: center; margin-bottom: 2rem;">Các sản phẩm bán chạy</h3>
<div class="container">
 <div class="sliderautoplay">
  @foreach($best_sellers as $bs)
  <div class='col-sm-4 thumbnail' style='margin-right: 1.5rem; width: 30%;'>
    <div class='f'>
      <img src='{{$bs->image}}' class='e' style="border-bottom: 1px solid;">
      <div class='caption'>
        <p>{{$bs->name}}</p>
        <span style='color: #CD3C0C'>{{$bs->price}}VNĐ</span><a href='details/{{$bs->id}}'  target='_blank'>
          <button style=" background-color: #fee5ed; border-color: #fee5ed;border-radius: 5px;border: 1px solid #F790FF;float: right;">Xem chi tiết</button>          
        </a>
      </div>
    </div>
  </div> 
  @endforeach 
</div>
</div>
@endsection
