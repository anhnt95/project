@extends('master2')
@section('contact')
<header class="masthead text-white text-center">
  <div class="overlay" style="background-image: url('source/landingpage/img/1794.jpg') !important; background-size: cover;"></div>
  <div class="row">
    <div class="col-sm-8 mx-auto">
        <h1 style="font-size: 30px; font-family: roboto;">Cần tư vấn? Liên hệ chúng tôi</h1>
        <form class="form-horizontal" action="lienhe" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}";>
            <fieldset>
            <div class="row">
                <div class="col-sm-3"></div>
            <div class="col-sm-3"> 
                <div class="control-group">
                    <label for="name" class="control-label" style="font-size: 20px;">Họ tên</label>
                    <div class="controls">
                        <input type="text" id="name" name="name">
                    </div>
                </div>
            </div>
            <div class="col-sm-3"> 
                <div class="control-group">
                    <label for="name" class="control-label" style="font-size: 20px;">Email*</label>
                    <div class="controls">
                        <input type="email" id="email" name="email" required>
                    </div>
                </div>
            </div>
            </div>
                <div class="control-group" style="padding-top: 15px;">
                    <label for="name" class="control-label" style="font-size: 20px;">Nội dung*</label>
                    <div class="controls">
                        <textarea class="required" rows="3" cols="48" id="massage" name="message" required></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                <div class="form-actions col-sm-2">
                     <button type="submit" value="submit" class="btn btn-block btn-lg btn-primary my-btn">Gửi</button>
                </div>
                </div>
            </fieldset> 
        </form>
    </div>
</div>
</header>

@endsection