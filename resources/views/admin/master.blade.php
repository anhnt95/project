<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<title>Admin</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/colors.css') }}" rel="stylesheet" type="text/css">
</head>
<body class="navbar-top">
	<!-- Main navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html">Quản lý sản phẩm</a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav navbar-right">

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ asset('images/placeholder.jpg') }} " alt="">
						<span>ADMIN</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="#"><i class="icon-coins"></i> My balance</a></li>
						<li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navigation -->


	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-fixed">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="{{ asset('admin/images/placeholder.jpg') }}" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">ADMIN</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Việt Nam
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li><a href="{!! route('admin.product.list') !!}"><i class="icon-home4"></i> <span>Home</span></a></li>
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>Category</span></a>
									<ul>
										<li><a href="{!! route('admin.cate.getAdd') !!}">Add Category</a></li>
										<li><a href="{!! route('admin.cate.list') !!}">List Category </a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-archive"></i> <span>Product</span></a>
									<ul>
										<li><a href="{!! route('admin.product.getAdd') !!}">Add Product</a></li>
										<li><a href="{!! route('admin.product.list') !!}">List Product</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-users"></i> <span>User</span></a>
									<ul>
										<li><a href="AddUser.html">Add User</a></li>
										<li><a href="ListUser.html">List User</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span></h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i>Home</a></li>
							
						</ul>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">	
					<!--Display flash -->
					@if(Session::has('flash_messages'))
						<div class="alert alert-{!! Session::get('flash_level') !!} alert-styled-left alert-bordered">
							<span class="text-semibold"> {!! Session::get('flash_messages') !!} </span>
						</div>
					@endif
					<!--End Displat -->
					<!--Chứa nội dung -->
					@yield('content')
					<!--Kết thúc nội dung -->

				</div>
				<!-- /Content area -->
		</div>
		<!-- /Page content -->
	</div>
<!-- /page container -->


	<script type="text/javascript" src="{{ asset('admin/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('admin/js/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/ckfinder/ckfinder.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/ui/nicescroll.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/maxlength.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/notifications/sweet_alert.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/passy.js') }}"></script>

	<script type="text/javascript" src="{{ asset('admin/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/pages/layout_fixed_custom.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/core/myscript.js') }}"></script>
</body>
</html>