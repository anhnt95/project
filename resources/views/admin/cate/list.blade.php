@extends('admin.master')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title"> List Category </h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
						<li><a data-action="close"></a></li>
					</ul>
				</div>
			</div>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="150px">STT</th>
						<th>Category Name</th>
						<th>Category Parent</th>
						<th class="text-center" width="70px">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $stt = 0?>
					@foreach($data as $item)
					<?php $stt = $stt + 1?>
					<tr>
						<td> <input name="txtPosition" type="text" class="text-center" value="{!! $stt !!}" style="width: 30px" data-id="1"></td>
						<td>{!! $item["name"] !!} </td>
						<td>
							@if($item["parent_id"]==0){!! "root" !!}
							@else
								<?php
									$parent = DB::table('cates')->where('id',$item['parent_id'])->first();
									echo $parent->name;
								?>		
							@endif	 
						</td>
						<td class="text-center">
							<ul class="icons-list">
								<li class="text-primary-600"><a href="{!! URL::route('admin.cate.getEdit', $item['id']) !!}" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i></a></li>
								<li class="text-danger-600"><a href="{!! URL::route('admin.cate.getDelete', $item['id']) !!}" data-popup="tooltip" title="Remove" class="sweet_warning"><i class="icon-trash"></i></a></li>
							</ul>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection()
