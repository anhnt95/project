@extends('admin.master')
@section('controller','Category')
@section('action','Add')
@section('content')
<div class="row">
	<form action="{!! route('admin.cate.getAdd') !!}" method="POST" accept-charset="utf-8">
		<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
		<div class="col-md-12">
			<div class="panel panel-body border-top-primary text-left">
				<button type="submit" name="btnSaveNew" value="btnSaveNew" class="btn btn-success btn-labeled"><b><i class="icon-add"></i></b> Save</button>
			</div>
		</div>
		<div class="col-md-12">
		@if( count($errors) > 0)	
			<div class="alert alert-danger alert-styled-left alert-bordered">
				<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				@foreach($errors->all() as $error)
					<span class="text-semibold"> {!! $error !!} </span>
				@endforeach
			</div>
		@endif
		</div>
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h6 class="panel-title">Add Category</h6>
					<div class="heading-elements">
						<ul class="icons-list">
							<li><a data-action="collapse" class=""></a></li>
							<li><a data-action="reload"></a></li>
							<li><a data-action="close"></a></li>
						</ul>
					</div>
				</div>

				<div class="panel-body" style="display: block;">
					<div class="tabbable tab-content-bordered">
						<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
							<li class="active"><a href="#vietnamese" data-toggle="tab" aria-expanded="true">ENTER INFORMATION</a></li>

						</ul>

						<div class="tab-content">
							<div class="tab-pane has-padding active" >
								<div class="form-group">
									<label class="control-label">Category Name</label>
									<input type="text" id="name-cate" name="txtCateName" class="form-control" placeholder="Please Enter Category Name" />
								</div>
								<div class="form-group">
									<label class="control-label">Description</label>
									<textarea class="form-control" rows="3" name="txtDescription"></textarea>

								</div>
								<div class="form-group">
									<label class="control-label">Keywords</label>
									<input type="text" id="Keywords" name="txtKeywords" class="form-control" placeholder="Please Enter Keywords" />
								</div>
								<div class="form-group" style="margin-bottom: 50px">
									<label class="control-label">Parent_id</label>
									<select class="form-control" name="sltParent">
										<option value="0">Please Enter Parent ID</option>
										<?php cate_parent($parent)?>
									</select>
								</div>
								<div class="checkbox checkbox-switch">
									<input type="checkbox" name="chkDisplay" data-on-color="success" data-off-color="danger" data-on-text="Display On" data-off-text="Display Off" class="switch" checked="checked" />
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection()

