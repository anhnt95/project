@extends('admin.master')
@section('content')
@section('action', 'Add')
@section('controller','Product')
<div class="row">
	<div class="col-md-12">
		<form action="{!! route('admin.product.getAdd') !!}" method="POST" accept-charset="utf-8">
			<input type="hidden" name="_token" value="{!! csrf_token() !!}" />

			<div class="col-md-12">
				<div class="panel panel-body border-top-primary text-left">
					<button type="submit" name="btnSaveNew" value="btnSaveNew" class="btn btn-success btn-labeled"><b><i class="icon-add"></i></b> Save</button>
				</div>
			</div>
			<div class="col-md-12">
			@if( count($errors) > 0)	
			<div class="alert alert-danger alert-styled-left alert-bordered">
				<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				@foreach($errors->all() as $error)
					<span class="text-semibold"> {!! $error !!} </span>
				@endforeach
			</div>
		@endif
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">Add Product</h6>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse" class=""></a></li>
								<li><a data-action="reload"></a></li>
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>

					<div class="panel-body" style="display: block;">
						<div class="tabbable tab-content-bordered">
							<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
								<li class="active"><a href="#vietnamese" data-toggle="tab" aria-expanded="true">ENTER INFORMATION</a></li>

							</ul>

							<div class="tab-content">
								<div class="tab-pane has-padding active" id="vietnamese">
									<div class="form-group">
										<label class="control-label">Product Name</label>
										<input type="text" name="txtNameProduct" class="form-control" placeholder="Please Enter Product Name" />
									</div>
									<div class="form-group">
										<label class="control-label">Price (VNĐ)</label>
										<input type="text" name="txtPriceProduct" class="form-control" placeholder="Please Enter Fee" />
									</div>
									<div class="form-group">
										<label class="control-label">Description</label>
										<textarea class="form-control" id ="Description" name="txtDescriptionProduct" rows="3"></textarea>	
									</div>
									<div class="form-group">
										<label class="control-label">Link</label>
										<input type="text" name="txtLink" class="form-control" placeholder="Please Enter Slug URL" />
									</div>
									<div class="form-group" style="margin-bottom: 50px">
										<label class="control-label">Keywords</label>
										<input type="text" id="txtkeywords" name="txtKeywords" class="form-control col-lg-6 maxlength-textarea" maxlength="70" placeholder="Please Enter Keywords" />
									</div>
									<div class="form-group">
										<label class="control-label">Skin_tone</label>
										<input type="text" name="txtSkintone" class="tags-input" placeholder="White,Black,yellow..." />
									</div>
									<div class="form-group">
										<label class="control-label">Skin_type</label>
										<input type="text" name="txtSkintype" class="tags-input" placeholder="White,Black,yellow..." />
									</div>
									<div class="form-group">
										<label class="control-label">Main Image</label><br />
										<input type="text" name="txtImage" class="form-control" placeholder="Please Enter URL Image" />
									</div>
									<div class="form-group">
										<label class="control-label">Faulty</label>
										<input type="text" name="txtFaulty" class="form-control" placeholder="" />
									</div>
									<div class="form-group">
										<label class="control-label">Brand</label>
										<input type="text" name="txtBrand" class="form-control" placeholder="Tom Ford,Chanel..." />
									</div>
									<div class="form-group">
										<label class="control-label">Origin</label>
										<input type="text" name="txtOrigin" class="form-control" placeholder="Anh,Pháp,Mỹ..." />
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">Add Product</h6>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse" class=""></a></li>
								<li><a data-action="reload"></a></li>
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>

					<div class="panel-body" style="display: block;">
						<div class="form-group">
							<label class="control-label">Category</label>
							<select name="sltParent" class="form-control">
								<option value="">Please Choose Category</option>
								<?php cate_parent($cate,0,"--",old('sltParent'))?>
							</select>
						</div>

						<div class="form-group">
							<label class="control-label">Age</label>
							<div class="input-group">
								<input type="text" name="txtAge" class="tags-input" placeholder="" />
							</div>
							<br />
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- /Content area -->
@endsection();