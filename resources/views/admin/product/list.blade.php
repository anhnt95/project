@extends('admin.master')
@section('content')
@section('controller','Product')

<!-- Content area -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">List Product</h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="reload"></a></li>
						<li><a data-action="close"></a></li>
					</ul>
				</div>
			</div>
			<table class="table table-bordered table-hover datatable-highlight">
				<thead>
					<tr>
						<th width="50px">STT</th>
						<th>Product Name</th>
						<th>Image</th>
						<th>Price</th>
						<th class="text-center" width="70px">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php $stt = 0?>
					@foreach($data as $item)
					<?php $stt = $stt + 1?>
					<tr>
						<td><input name="txtPosition" type="text" class="text-center" value="{!! $stt !!}" style="width: 30px" data-id="1"></td>
						<td>{!! $item["name"] !!}</td>
						<td><img src="{!! $item["image"] !!}" alt="#" style="width: 50px;  height: 50px
						"/></td>
						<td>{!! $item["price"] !!}</td>
						<td class="text-center">
							<ul class="icons-list">
								<li class="text-primary-600"><a href="{!! URL::route('admin.product.getEdit', $item['id']) !!}" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i></a></li>
								<li class="text-danger-600"><a href="{!! URL::route('admin.product.getDelete', $item['id']) !!}" data-popup="tooltip" title="Remove" class="sweet_warning"><i class="icon-trash"></i></a></li>

							</ul>
						</td>
					</tr>
					@endforeach					
				</tbody>
			</table>
		</div>
	</div>

	<!-- /Content area -->
	@endsection()
