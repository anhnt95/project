@extends('master')
@section('product1')
@foreach($products as $p)
<div class="container">
	<div class="row">
		<div class="col-sm-6" style="padding-bottom: 3rem; padding-top: 4rem;">
			<img src="{{$p->image}}" alt="{{$p->name}}" style="width: 85%; margin-left: 30px; border-spacing: 1rem;">
		</div>
		<div class="col-sm-6" style="">
		<h3 style="color: #000000; ">Chi tiết sản phẩm</h3>
			<a href="{{$p->link}}" target="_blank">
				<button style=" background-color: #fee5ed; border-color: #fee5ed;border-radius: 5px;border: 1px solid #F790FF;float: left; padding: 10px 30px;">Mua hàng</button>
			</a><br><br><br>
			<p><strong>Giá:</strong> {{$p->price}}VNĐ<br> <strong>Thương hiệu:</strong>  {{$p->brand}}<br> <strong>Xuất xứ:</strong>  {{$p->origin}}<br> <strong>Chi tiết:</strong> {{$p->description}}
			<br><strong>Công dụng:</strong>  {{$p->function}}</p>
			<p></p>
		</div>
	</div>
</div>
@endforeach
@endsection