<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cates extends Model
{
	protected $table = "cates";
    public function product(){
    	return $this->hasMany('App\Products','cate_id','id');
    }
}
