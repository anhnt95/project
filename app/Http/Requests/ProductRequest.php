<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtProductName' => 'required|unique:products,name'
        ];
    }
    public function messages(){
        return[
            'txtProductName.required' => 'Please Enter Product Name',
            'txtProductName.unique' => 'This Name Product Is Exist'
        ];
    }

}
