<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use Mail;

class PageController extends Controller
{
    public function getLandingpage(){
    	return view('landingpage');
    }

    public function getContact(){
      return view('lienhe');
    }

    public function postContact(request $request){
      $data = [
        'ten' => request()->input('name'),
        'email' => request()->input('email'),
        'noidung' => request()->input('message')
      ];
      Mail::send('blanks',$data,function($message){
        $message->from('nhunguyen.web@gmail.com','Nhu Nguyen');
        $message->to('nhunguyen.web@gmail.com','Nhu Nguyen')->subject('Filterweb');
      });
      echo "<script>
        alert('Cám ơn bạn! Chúng tôi sẽ liên hệ lại trong thời gian sớm nhất');
        window.location = '".url('index')."'
      </script>";
    }

  	public function getCate($cate_id)
   {
      $best_sellers = products::where('best_sellers',1)->get();
      $products = products::where('cate_id', $cate_id)->paginate(12);
        // var_dump($products);
        // die();
      return view('cate',compact('products','best_sellers'));
   }

   public function getDetails($id)
    {
    	$products = products::where('id', $id)->get();
    	return view('details',compact('products'));
    }

   public function timkiem(request $request)
   {
      $keywords = $request->keywords;
      $best_sellers = products::where('best_sellers',1)->get();
      $products = products::where('keywords','like',"%$keywords%")->orwhere('name','like',"%$keywords%")->orwhere('faculty','like',"%$keywords%")->orwhere('description','like',"%$keywords%")->paginate(12);
      return view('timkiem',compact('products','best_sellers','keywords'));
   }

 public function loc(request $request)
   {
      $brand = $request->brand;
      $origin = $request->origin;
      $min_price = $request->min_price;
      $max_price = $request->max_price;
      $skin_type = $request->skin_type;
      $skin_tone = $request->skin_tone;
      $age = $request->age;
      $loaisanpham = $request->loaisanpham;
      $best_sellers = products::where('best_sellers',1)->get();
      $products = products::where('brand','like',"%$brand%")->where('price','>=',"$min_price")->where('origin','like',"%$origin%")->where('skin_type','like',"%$skin_type%")->where('skin_tone','like',"%$skin_tone%")->where('age','like',"$age")->where('price','<=',"$max_price")->where('keywords','like',"%$loaisanpham%")->get();
      return view('loc',compact('products','best_sellers','skin_tone','skin_type','age','max_price','min_price','brand','origin','loaisanpham'));
   }
    
  // public function loc(request $request)
  //  {
  //     $brand = $request->brand;
  //     $origin = $request->origin;
  //     $min_price = $request->min_price;
  //     $max_price = $request->max_price;
  //     $skin_type = $request->skin_type;
  //     $skin_tone = $request->skin_tone;
  //     $age = $request->age;
  //     $products = (new products)->newQuery();
  //     $best_sellers = products::where('best_sellers',1)->get();
  //     if($request->has('brand'))
  //       $products = products::where('brand', 'like',"%$brand%")->get();
  //     if($request->has('origin'))
  //       $products = products::where('origin','like',"%$origin%")->get();
  //     if($request->has('min_price'))
  //       $products = products::where('price','>',"$min_price")->get();
  //     if($request->has('max_price'))
  //       $products = products::where('price','<',"$max_price")->get();
  //     if($request->has('skin_tone'))
  //       $products = products::where('skin_tone','like',"%$skin_tone%")->orwhere('skin_tone','all')->get();
  //     if($request->has('skin_type'))
  //       $products = products::where('skin_type','like',"%$skin_type%")->orwhere('skin_type','all')->get();
  //     if($request->has('age'))
  //       $products = products::where('age','all')->get();
  //     //$products = products::where('brand','like',"%$brand%")->where('price','>',"$min_price")->where('origin','like',"%$origin%")->where('skin_type','like',"%$skin_type%")->where('skin_tone','like',"%$skin_tone%")->where('age','like',"%$age%")->where('price','<',"$max_price")->get();
  //     return $products->get();
  //  }

public function cate(){
  $products = products::paginate(12);
  $best_sellers = products::where('best_sellers',1)->get();
  return view('cate',compact('products','best_sellers'));
}

}