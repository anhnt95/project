<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CateRequest;
use App\Cates;
class CateController extends Controller
{
    public function getAdd(){
    	$parent = Cates::select('id','name','parent_id')->get()->toArray();
        return view('admin.cate.add', compact('parent'));
    }
    public function getList(){
        $data = Cates::select('id','name','parent_id')->orderBy('id', 'DESC')->get()->toArray();
        return view('admin.cate.list', compact('data'));
    }
    public function getDelete($id){
        $parent =  Cates::where('parent_id',$id)->count();
        if($parent == 0){
        $cate = Cates::find($id);
        $cate->delete($id);
        return redirect()->route('admin.cate.list')->with(['flash_level'=>'success', 'flash_messages'=>'Success !! Complete Delete Category']);
        } else{
            echo "<script type='text/javascript'>
                alert('Sorry! You cant delete this category');
                window.location='";
                echo route('admin.cate.list');
            echo"'
            </script>";
        }
    }

    public function getEdit($id){
        $data = Cates::findOrFail($id)->toArray();
        $parent = Cates::select('id','name','parent_id')->get()->toArray();
        return view('admin.cate.edit', compact('parent','data','id'));
    }

    public function postEdit(Request $request, $id){
        $this->validate($request,
            ['txtCateName'=> 'required'],
            ['txtCateName.required'=>'Please Enter Name Category Edit']
        );
        $cate = Cates::find($id);
        $cate->name = $request->txtCateName;
        $cate->keywords = $request->txtKeywords;
        $cate->description = $request->txtDescription;
        $cate->parent_id = $request->sltParent;
        $cate->display = $request->chkDisplay;
        $cate->save();
        return redirect()->route('admin.cate.list')->with(['flash_level'=>'success', 'flash_messages'=>'Success !! Complete Update Category']);

    }
    public function postAdd (CateRequest $request){
    	$cate = new cates;
    	$cate->name = $request->txtCateName;
    	$cate->keywords = $request->txtKeywords;
    	$cate->description = $request->txtDescription;
    	$cate->parent_id = $request->sltParent;
    	$cate->display = $request->chkDisplay;
    	$cate->save();
        return redirect()->route('admin.cate.list')->with(['flash_level'=>'success', 'flash_messages'=>'Success !! Complete Add Category']);
    }
}
