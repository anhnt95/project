<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Products;
use App\Cates;

class ProductController extends Controller
{
     public function getAdd(){
     	$cate = Cates::select('id','name','parent_id')->get()->toArray();
        return view('admin.product.add', compact('cate'));
     }
     public function postAdd(Request $request){
     	$product = new products;
     	$product->name = $request->txtNameProduct;
     	$product->price = $request->txtPriceProduct;
     	$product->description = $request->txtDescriptionProduct;
     	$product->image =  $request->txtImage;
     	$product->keywords =  $request->txtKeywords;
     	$product->skin_tone =  $request->txtSkintone;
     	$product->skin_type =  $request->txtSkintype;
     	$product->age =  $request->txtAge;
     	$product->faculty =  $request->txtFaulty;
     	$product->brand =  $request->txtBrand;
     	$product->origin =  $request->txtOrigin;
     	$product->link =  $request->txtLink;
     	$product->spf_pa =  "50++";
     	$product->user_id =  1;
     	$product->cate_id =  $request->sltParent;
     	$product->best_sellers =  0;
     	$product->save();
     	return redirect()->route('admin.product.list')->with(['flash_level'=>'success', 'flash_messages'=>'Success !! Complete Add Product']);
     }
      public function getList(){
        $data = products::select('id','name','image', 'price')->orderBy('id', 'DESC')->get()->toArray();
        return view('admin.product.list', compact('data'));
    }
    public function getDelete($id){
        $product =  products::find($id);
        $product->delete($id);
        return redirect()->route('admin.product.list')->with(['flash_level'=>'success', 'flash_messages'=>'Success !! Complete Delete Product']);
    }

     public function getEdit($id){
        $data = products::findOrFail($id)->toArray();
       $cate = Cates::select('id','name','parent_id')->get()->toArray();

         return view('admin.product.edit', compact('data','cate'));
    }

    public function postEdit(Request $request, $id){
        $this->validate($request,
            ['txtProductName'=> 'required'],
            ['txtProductName.required'=>'Please Enter Name Product Edit']
        );
     $product = products::find($id);
     $product->name = $request->txtNameProduct;
     $product->price = $request->txtPriceProduct;
     $product->description = $request->txtDescriptionProduct;
     $product->image =  $request->txtImage;
     $product->keywords =  $request->txtKeywords;
     $product->skin_tone =  $request->txtSkintone;
     $product->skin_type =  $request->txtSkintype;
     $product->age =  $request->txtAge;
     $product->faculty =  $request->txtFaulty;
     $product->brand =  $request->txtBrand;
     $product->origin =  $request->txtOrigin;
     $product->link =  $request->txtLink;
     $product->spf_pa =  "50++";
     $product->user_id =  1;
     $product->cate_id =  $request->sltParent;
     $product->best_sellers =  0;
     $product->save();
        return redirect()->route('admin.product.list')->with(['flash_level'=>'success', 'flash_messages'=>'Success !! Complete Update Product']);

    }
}
