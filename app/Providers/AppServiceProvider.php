<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Cates;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         // view()->composer('landingpage',function($view){
         //     $category = Cates::all();
         //     $view->with('category',$category);
         // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
