<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    protected $table = "products";

    public function cates(){
    	return $this->belongsTo('App\Cates','cate_id','id');
    }

    public function product_image(){
    	return $this->hasMany('App\ProductImage','product_id','id');
    }

    public function users(){
    	return $this->belongsTo('App\User','user_id','id');
    }
}
