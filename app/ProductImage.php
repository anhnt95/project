<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = "product_image";

    public function products(){
    	return $this->belongsTo('App\Products','product_id','id');
}
